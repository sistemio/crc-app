# Tutorial de traducción

[TOC]

## Modificar una traducción

Traducir en CakePHP es muy sencillo ya que él mismo nos genera los archivos
"pot" para trabajar con ellos. Por lo tanto únicamente tendremos que editar
estos archivos. ¿Cómo? Lo dividiré en pasos:

1.  Debemos de utilizar un editor de archivos "po" (para facilitar
    el trabajo). Por ejemplo: [PoEdit](http://www.poedit.net) (Recomendado por
    CakePHP)

2.  Abrimos el programa. Entramos en *Archivo->Abrir...* y
    abrimos el archivo **default.po**.

3.  Una vez abierto el archivo podremos ir seleccionando las 
    frases en inglés y traducirlas al idioma que deseemos.
    
    *Nota: Tengamos en cuenta en qué carpeta esta guardado el archivo
    **default.po**. Si es **/spa/LC_MESSAGES** tendremos que traducirlo al
    castellano.*

    [![PoEdit][PoEdit_abierto]][PoEdit_abierto]

Para traducir de una manera mas cómoda con el PoEdit simplemente con el
tabulador podeis seleccionar la frase y a su vez traducir.

## Obtener archivos .pot

Por si alguien lo desea a continuación voy a explicar como obtener los archivos 
"pot" a partir de la consola de CakePHP:

1.  Por comandos nos metemos dentro de la dirección donde se 
    encuentra nuestra aplicación Cake dentro de la carpeta */app*.
    
    *Nota: los comandos posibles para esto son: `cd` (Linux y MAC) y
    `dir` (Windows).*

2.  Ejecutamos el siguiente comando:

    ```
    $ ./Console/cake i18n
    ```

3.  Seguimos las instrucciones del programa y tendremos los
    archivos "pot" para editarlos en la dirección que le hayamos indicado.

Espero que haya sido de ayuda.   

[PoEdit_abierto]: ./img/Poedit_open.png