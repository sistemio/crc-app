# Flujo de trabajo

[TOC]

## Notificaciones

Que cada usuario se [configure las notificaciones][notifications] como prefiera,
pero tal vez sería recomendable que todos los desarrolladores marquen
la casilla *todas las incidencias* del menú de seguimiento del repositorio,
que se puede ver en el dibujo del ojo que hay arriba a la derecha, tal
como en la imagen.

![Menú de observación del repositorio][observe]

[notifications]: https://confluence.atlassian.com/display/BITBUCKET/Manage+Inbox+and+email+notifications
[observe]: https://confluence.atlassian.com/download/attachments/304578655/repo-wide-notifs.png?version=2&modificationDate=1379979292097&api=v2&effects=drop-shadow

## Git y el control de versiones

### Conceptos básicos

Git es el sistema de control de versiones distribuido elegido para el proyecto.

La wikipedia contiene una [guía muy completa en español de qué son, cómo
funcionan y para qué sirven los sistemas de control de
versiones][scm], que es un buen punto de partida para los que desconocen
el tema.

[scm]: es.wikipedia.org/wiki/Control_de_versiones

### Tutoriales

-   Básico y fácil, en español:
    http://rogerdudler.github.io/git-guide/index.es.html
-   De los creadores de Bitbucket, en inglés:
    https://www.atlassian.com/git/tutorial
-   De los creadores de Git, en español:
    http://git-scm.com/book/es

### Interfaz gráfica

La mayoría de tutoriales que encuentres se referirán a la línea de comandos de
Git, pero eso no quiere decir que sea la única manera de usarlo.

Si prefieres ventanas y botones, hay varias opciones:

-   Los comandos `git gui` y `gitk` vienen de fábrica. Feos, pero completísimos.

-   [Git-Cola](http://git-cola.github.io/) es otra buena opción.

-   [Más opciones](http://git-scm.com/downloads/guis).

## Estilo de escritura

Se acordó utilizar los [estándares de escritura de código de PEAR][pear-std].
Básicamente:

-   Indentar a 4 espacios.

-   Intentar que las líneas no superen los 80 caracteres.

-   Codificar los ficheros en UTF-8.

-   Que se lea bien.

[pear-std]: http://pear.php.net/manual/en/standards.php

## Cambiar el código fuente

Se explica perfectamente en [Bitbucket 101][instructions], pero aquí va un
pequeño resumen:

1.  Necesitarás tener una cuenta en Bitbucket.

2.  Visita el repositorio principal y [bifúrcalo][fork]. Esto creará una copia
    para trabajar desde tu propia cuenta. También se le llama *bifurcación*,
    *tenedor*, *clon*, *división* o *fork*.

    [![Foto de cómo bifurcar en Bitbucket.][fork-img]][fork-img]

3.  Clona **tu bifurcación** en tu ordenador, usando la línea de comandos que te
    da Bitbucket al usar el botón *clonar*.

    [![Foto de cómo clonar en Bitbucket.][clone-img]][clone-img]

4.  [Crea una *rama*][branch] con un nombre que indique en qué
    *característica* o *incidencia* vas a trabajar.

5.  Ve haciendo modificaciones necesarias en esa rama.

6.  Comprueba que las modificaciones van funcionando bien.

7.  Ve consignando (`git commit`) los cambios que ves que funcionan en esa rama.
    **Es mejor tener muchos *commit* pequeños que pocos muy grandes**.

8.  Puedes ir enviándolo (`git push`) a tu bifurcación creada en el paso 2.

9.  Repite los pasos 4 a 7 cuanto sea necesario.

10. Mientras tanto es posible que el repositorio principal haya recibido más
    modificaciones, así que tendrás que [mezclarlas][merge] en el tuyo
    y resolver los posibles conflictos.

    ![Comparar con el repositorio principal e importar sus cambios][compare]

10. Cuando esté terminado tu trabajo, envíalo al repositorio principal:

    1.  Entra en tu bifurcación en Bitbucket.

    2.  Pulsa el botón *solicitud de integración*, también llamado *pull
        request*, y completa la solicitud.

        ![Completar la solicitud de integración][pull-request]

    3.  Si se detectan fallos al revisar tu solicitud, se te pedirá que los
        corrijas. Deberás estar atento a las revisiones que se te hagan.

    4.  Cuando tu solicitud esté correcta, se mezclará con el repositorio
        principal y el resto de desarrolladores recibirán tus cambios.

[instructions]: https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101
[fork]: https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository
[fork-img]: ./img/bitbucket-fork.jpg
[clone-img]: ./img/bitbucket-clone.jpg
[branch]: https://confluence.atlassian.com/display/BITBUCKET/Branching+a+Repository
[merge]: https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo%2C+Compare+Code%2C+and+Create+a+Pull+Request#ForkaRepo,CompareCode,andCreateaPullRequest-Step1.Forkanotheruser%27srepo
[compare]: https://confluence.atlassian.com/download/attachments/304578655/top_compare.png?version=4&modificationDate=1379979369198&api=v2&effects=drop-shadow
[pull-request]: https://confluence.atlassian.com/download/attachments/304578655/pull_request_form.png?version=3&modificationDate=1379979261777&api=v2&effects=drop-shadow
