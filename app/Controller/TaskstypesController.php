<?php
App::uses('AppController', 'Controller');
/**
 * Taskstypes Controller
 *
 * @property Taskstype $Taskstype
 */
class TaskstypesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Taskstype->recursive = 0;
		$this->set('taskstypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Taskstype->exists($id)) {
			throw new NotFoundException(__('Invalid taskstype'));
		}
		$options = array('conditions' => array('Taskstype.' . $this->Taskstype->primaryKey => $id));
		$this->set('taskstype', $this->Taskstype->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Taskstype->create();
			if ($this->Taskstype->save($this->request->data)) {
				$this->Session->setFlash(__('The taskstype has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The taskstype could not be saved. Please, try again.'));
			}
		}
		$repeats = $this->Taskstype->Repeat->find('list');
		$phases = $this->Taskstype->Phase->find('list');
		$parentTaskstypes = $this->Taskstype->ParentTaskstype->find('list');
		$projects = $this->Taskstype->Project->find('list');
		$departments = $this->Taskstype->Department->find('list');
		$this->set(compact('repeats', 'phases', 'parentTaskstypes', 'projects', 'departments'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Taskstype->exists($id)) {
			throw new NotFoundException(__('Invalid taskstype'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Taskstype->save($this->request->data)) {
				$this->Session->setFlash(__('The taskstype has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The taskstype could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Taskstype.' . $this->Taskstype->primaryKey => $id));
			$this->request->data = $this->Taskstype->find('first', $options);
		}
		$repeats = $this->Taskstype->Repeat->find('list');
		$phases = $this->Taskstype->Phase->find('list');
		$parentTaskstypes = $this->Taskstype->ParentTaskstype->find('list');
		$projects = $this->Taskstype->Project->find('list');
		$departments = $this->Taskstype->Department->find('list');
		$this->set(compact('repeats', 'phases', 'parentTaskstypes', 'projects', 'departments'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Taskstype->id = $id;
		if (!$this->Taskstype->exists()) {
			throw new NotFoundException(__('Invalid taskstype'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Taskstype->delete()) {
			$this->Session->setFlash(__('Taskstype deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Taskstype was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
