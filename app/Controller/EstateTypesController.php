<?php
App::uses('AppController', 'Controller');
/**
 * EstateTypes Controller
 *
 * @property EstateType $EstateType
 */
class EstateTypesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->EstateType->recursive = 0;
		$this->set('estateTypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EstateType->exists($id)) {
			throw new NotFoundException(__('Invalid estate type'));
		}
		$options = array('conditions' => array('EstateType.' . $this->EstateType->primaryKey => $id));
		$this->set('estateType', $this->EstateType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->EstateType->create();
			if ($this->EstateType->save($this->request->data)) {
				$this->Session->setFlash(__('The estate type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The estate type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EstateType->exists($id)) {
			throw new NotFoundException(__('Invalid estate type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->EstateType->save($this->request->data)) {
				$this->Session->setFlash(__('The estate type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The estate type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EstateType.' . $this->EstateType->primaryKey => $id));
			$this->request->data = $this->EstateType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->EstateType->id = $id;
		if (!$this->EstateType->exists()) {
			throw new NotFoundException(__('Invalid estate type'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->EstateType->delete()) {
			$this->Session->setFlash(__('Estate type deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Estate type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
