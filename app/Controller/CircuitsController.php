<?php
	App::uses('AppController', 'Controller');
	
	/** ******************************************************************************************************************************************	**
	** Circuits Controller																																										**
	**																																																		**
	** @property Circuit $Circuit																																						**
	 *********************************************************************************************************************************************	**/
	class CircuitsController extends AppController {

		public $paginate = array(
			'limit' => 25
		);

		/** ***************************************************************************************************************************************	**
		 ** index METODO 																																									**
		 **																																																**
		 ** @return void                                                                                                                                                             				**
		 ******************************************************************************************************************************************	**/
		public function index() {
			$this->Circuit->recursive = 0;
			$this->set('circuits', $this->paginate());
		}

	
		/** ***************************************************************************************************************************************	**
		 ** view method																																											**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function view($id = null) {
			if (!$this->Circuit->exists($id)) {
				throw new NotFoundException(__('Invalid circuit'));
			}
			$options = array('conditions' => array('Circuit.' . $this->Circuit->primaryKey => $id));
			$this->set('circuit', $this->Circuit->find('first', $options));
		}

		/** ***************************************************************************************************************************************	**
		 * add method																																											**
		 *																																																	**
		 * @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function add() {
			if ($this->request->is('post')) {
				$this->Circuit->create();
				
				if ($this->Circuit->save($this->request->data)) {
					$this->Session->setFlash(__('Circuit has been saved'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Circuit could not be saved, try again.'));
				}
			}
			
			$districts = $this->Circuit->District->find('list');
			$this->set(compact('districts'));
		}

		/** ***************************************************************************************************************************************	**
		 ** edit method																																											**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function edit($id = null) {
			if (!$this->Circuit->exists($id)) {
				throw new NotFoundException(__('Invalid circuit'));
			}
			
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->Circuit->save($this->request->data)) {
					$this->Session->setFlash(__('Circuit has been saved.'));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Circuit could not be saved, try again.'));
				}
			} else {
				$options = array('conditions' => array('Circuit.' . $this->Circuit->primaryKey => $id));
				$this->request->data = $this->Circuit->find('first', $options);
			}
			
			$districts = $this->Circuit->District->find('list');
			$this->set(compact('districts'));
		}

		/** ***************************************************************************************************************************************	**
		 ** delete method																																										**
		 **																																																**
		 ** @throws NotFoundException																																				**
		 ** @param string $id																																								**
		 ** @return void																																											**
		 ******************************************************************************************************************************************	**/
		public function delete($id = null) {
			$this->Circuit->id = $id;
			if (!$this->Circuit->exists()) {
				throw new NotFoundException(__('Invalid circuit'));
			}
			
			$this->request->onlyAllow('post', 'delete');
			if ($this->Circuit->delete()) {
				$this->Session->setFlash(__('Circuit deleted'));
				$this->redirect(array('action' => 'index'));
			}
			
			$this->Session->setFlash(__('Circuit was not deleted'));
			$this->redirect(array('action' => 'index'));
		}
	}
?>