<?php
App::uses('AppController', 'Controller');
/**
 * CongregationsEstates Controller
 *
 * @property CongregationsEstate $CongregationsEstate
 */
class CongregationsEstatesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CongregationsEstate->recursive = 0;
		$this->set('congregationsEstates', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CongregationsEstate->exists($id)) {
			throw new NotFoundException(__('Invalid congregations estate'));
		}
		$options = array('conditions' => array('CongregationsEstate.' . $this->CongregationsEstate->primaryKey => $id));
		$this->set('congregationsEstate', $this->CongregationsEstate->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CongregationsEstate->create();
			if ($this->CongregationsEstate->save($this->request->data)) {
				$this->Session->setFlash(__('The congregations estate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The congregations estate could not be saved. Please, try again.'));
			}
		}
		$congregations = $this->CongregationsEstate->Congregation->find('list');
		$estates = $this->CongregationsEstate->Estate->find('list');
		$this->set(compact('congregations', 'estates'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CongregationsEstate->exists($id)) {
			throw new NotFoundException(__('Invalid congregations estate'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->CongregationsEstate->save($this->request->data)) {
				$this->Session->setFlash(__('The congregations estate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The congregations estate could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CongregationsEstate.' . $this->CongregationsEstate->primaryKey => $id));
			$this->request->data = $this->CongregationsEstate->find('first', $options);
		}
		$congregations = $this->CongregationsEstate->Congregation->find('list');
		$estates = $this->CongregationsEstate->Estate->find('list');
		$this->set(compact('congregations', 'estates'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CongregationsEstate->id = $id;
		if (!$this->CongregationsEstate->exists()) {
			throw new NotFoundException(__('Invalid congregations estate'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->CongregationsEstate->delete()) {
			$this->Session->setFlash(__('Congregations estate deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Congregations estate was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
