<?php
App::uses('TasksController', 'Controller');

/**
 * TasksController Test Case
 *
 */
class TasksControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.task',
		'app.repeat',
		'app.taskstype',
		'app.phase',
		'app.project_type',
		'app.project',
		'app.estate',
		'app.estate_type',
		'app.congregation',
		'app.circuit',
		'app.district',
		'app.congregations_estate',
		'app.assistance',
		'app.user',
		'app.order',
		'app.department',
		'app.line',
		'app.tasks_user'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
