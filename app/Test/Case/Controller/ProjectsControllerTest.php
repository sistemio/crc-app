<?php
App::uses('ProjectsController', 'Controller');

/**
 * ProjectsController Test Case
 *
 */
class ProjectsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project',
		'app.estate',
		'app.estate_type',
		'app.congregation',
		'app.circuit',
		'app.district',
		'app.congregations_estate',
		'app.project_type',
		'app.phase',
		'app.task',
		'app.taskstype',
		'app.assistance',
		'app.user',
		'app.order'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
