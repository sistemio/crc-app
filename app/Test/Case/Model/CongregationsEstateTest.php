<?php
App::uses('CongregationsEstate', 'Model');

/**
 * CongregationsEstate Test Case
 *
 */
class CongregationsEstateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.congregations_estate',
		'app.congretation',
		'app.estate'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CongregationsEstate = ClassRegistry::init('CongregationsEstate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CongregationsEstate);

		parent::tearDown();
	}

}
