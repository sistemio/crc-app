<?php
App::uses('DocType', 'Model');

/**
 * DocType Test Case
 *
 */
class DocTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.doc_type',
		'app.doc'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DocType = ClassRegistry::init('DocType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DocType);

		parent::tearDown();
	}

}
