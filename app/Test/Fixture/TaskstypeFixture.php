<?php
/**
 * TaskstypeFixture
 *
 */
class TaskstypeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'start' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'end' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'limit' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'repeat_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'phase_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'key' => 'index'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'key' => 'index'),
		'project_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'key' => 'index'),
		'department_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'parent_id_idx' => array('column' => 'parent_id', 'unique' => 0),
			'project_id_idx' => array('column' => 'project_id', 'unique' => 0),
			'phase_id_idx' => array('column' => 'phase_id', 'unique' => 0),
			'department_id_idx' => array('column' => 'department_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'start' => '2013-08-05 19:45:43',
			'end' => '2013-08-05 19:45:43',
			'limit' => '2013-08-05 19:45:43',
			'repeat_id' => 1,
			'created' => '2013-08-05 19:45:43',
			'modified' => '2013-08-05 19:45:43',
			'phase_id' => 1,
			'parent_id' => 1,
			'project_id' => 1,
			'department_id' => 1
		),
	);

}
