<?php
/**
 * AssistanceFixture
 *
 */
class AssistanceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'project_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10),
		'checked' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'start' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'project_id' => 1,
			'checked' => 1,
			'start' => 'Lorem ipsum dolor sit amet'
		),
	);

}
