<?php
App::uses('AppModel', 'Model');
/**
 * Congregation Model
 *
 * @property Circuit $Circuit
 * @property Estate $Estate
 */
class Congregation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'circuit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Circuit' => array(
			'className' => 'Circuit',
			'foreignKey' => 'circuit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Estate' => array(
			'className' => 'Estate',
			'joinTable' => 'congregations_estates',
			'foreignKey' => 'congregation_id',
			'associationForeignKey' => 'estate_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
