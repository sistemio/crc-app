<?php
App::uses('AppModel', 'Model');
/**
 * EstateType Model
 *
 * @property Estate $Estate
 */
class EstateType extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Estate' => array(
			'className' => 'Estate',
			'foreignKey' => 'estate_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
