<?php
App::uses('AppModel', 'Model');
/**
 * CongregationsEstate Model
 *
 * @property Congregation $Congregation
 * @property Estate $Estate
 */
class CongregationsEstate extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Congregation' => array(
			'className' => 'Congregation',
			'foreignKey' => 'congregation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Estate' => array(
			'className' => 'Estate',
			'foreignKey' => 'estate_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
