<div class="taskstypes view">
<h2><?php echo __('Taskstype'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Limit'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['limit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Repeat'); ?></dt>
		<dd>
			<?php echo $this->Html->link($taskstype['Repeat']['name'], array('controller' => 'repeats', 'action' => 'view', $taskstype['Repeat']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($taskstype['Taskstype']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phase'); ?></dt>
		<dd>
			<?php echo $this->Html->link($taskstype['Phase']['name'], array('controller' => 'phases', 'action' => 'view', $taskstype['Phase']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Taskstype'); ?></dt>
		<dd>
			<?php echo $this->Html->link($taskstype['ParentTaskstype']['name'], array('controller' => 'taskstypes', 'action' => 'view', $taskstype['ParentTaskstype']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project'); ?></dt>
		<dd>
			<?php echo $this->Html->link($taskstype['Project']['name'], array('controller' => 'projects', 'action' => 'view', $taskstype['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Department'); ?></dt>
		<dd>
			<?php echo $this->Html->link($taskstype['Department']['name'], array('controller' => 'departments', 'action' => 'view', $taskstype['Department']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Taskstype'), array('action' => 'edit', $taskstype['Taskstype']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Taskstype'), array('action' => 'delete', $taskstype['Taskstype']['id']), null, __('Are you sure you want to delete # %s?', $taskstype['Taskstype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Taskstype'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Repeats'), array('controller' => 'repeats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Repeat'), array('controller' => 'repeats', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phases'), array('controller' => 'phases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phase'), array('controller' => 'phases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('controller' => 'taskstypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Taskstypes'); ?></h3>
	<?php if (!empty($taskstype['ChildTaskstype'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Limit'); ?></th>
		<th><?php echo __('Repeat Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Phase Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Department Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($taskstype['ChildTaskstype'] as $childTaskstype): ?>
		<tr>
			<td><?php echo $childTaskstype['id']; ?></td>
			<td><?php echo $childTaskstype['name']; ?></td>
			<td><?php echo $childTaskstype['start']; ?></td>
			<td><?php echo $childTaskstype['end']; ?></td>
			<td><?php echo $childTaskstype['limit']; ?></td>
			<td><?php echo $childTaskstype['repeat_id']; ?></td>
			<td><?php echo $childTaskstype['created']; ?></td>
			<td><?php echo $childTaskstype['modified']; ?></td>
			<td><?php echo $childTaskstype['phase_id']; ?></td>
			<td><?php echo $childTaskstype['parent_id']; ?></td>
			<td><?php echo $childTaskstype['project_id']; ?></td>
			<td><?php echo $childTaskstype['department_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'taskstypes', 'action' => 'view', $childTaskstype['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'taskstypes', 'action' => 'edit', $childTaskstype['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'taskstypes', 'action' => 'delete', $childTaskstype['id']), null, __('Are you sure you want to delete # %s?', $childTaskstype['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Child Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
