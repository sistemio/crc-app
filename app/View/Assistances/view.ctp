<div class="assistances view">
<h2><?php echo __('Assistance'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($assistance['Assistance']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($assistance['User']['name'], array('controller' => 'users', 'action' => 'view', $assistance['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project'); ?></dt>
		<dd>
			<?php echo $this->Html->link($assistance['Project']['name'], array('controller' => 'projects', 'action' => 'view', $assistance['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Checked'); ?></dt>
		<dd>
			<?php echo h($assistance['Assistance']['checked']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start'); ?></dt>
		<dd>
			<?php echo h($assistance['Assistance']['start']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Assistance'), array('action' => 'edit', $assistance['Assistance']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Assistance'), array('action' => 'delete', $assistance['Assistance']['id']), null, __('Are you sure you want to delete # %s?', $assistance['Assistance']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Assistances'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assistance'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
