<div class="projects view">
<h2><?php echo __('Project'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($project['Project']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($project['Project']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($project['Project']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Estate'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Estate']['name'], array('controller' => 'estates', 'action' => 'view', $project['Estate']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['ProjectType']['name'], array('controller' => 'project_types', 'action' => 'view', $project['ProjectType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Time'); ?></dt>
		<dd>
			<?php echo h($project['Project']['start_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Time'); ?></dt>
		<dd>
			<?php echo h($project['Project']['end_time']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project'), array('action' => 'edit', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project'), array('action' => 'delete', $project['Project']['id']), null, __('Are you sure you want to delete # %s?', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estates'), array('controller' => 'estates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('controller' => 'estates', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Types'), array('controller' => 'project_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Type'), array('controller' => 'project_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Assistances'), array('controller' => 'assistances', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Assistance'), array('controller' => 'assistances', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tasks'), array('controller' => 'tasks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Task'), array('controller' => 'tasks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Taskstypes'), array('controller' => 'taskstypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Assistances'); ?></h3>
	<?php if (!empty($project['Assistance'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Checked'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['Assistance'] as $assistance): ?>
		<tr>
			<td><?php echo $assistance['id']; ?></td>
			<td><?php echo $assistance['user_id']; ?></td>
			<td><?php echo $assistance['project_id']; ?></td>
			<td><?php echo $assistance['checked']; ?></td>
			<td><?php echo $assistance['start']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'assistances', 'action' => 'view', $assistance['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'assistances', 'action' => 'edit', $assistance['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'assistances', 'action' => 'delete', $assistance['id']), null, __('Are you sure you want to delete # %s?', $assistance['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Assistance'), array('controller' => 'assistances', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Orders'); ?></h3>
	<?php if (!empty($project['Order'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Validated'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['Order'] as $order): ?>
		<tr>
			<td><?php echo $order['id']; ?></td>
			<td><?php echo $order['name']; ?></td>
			<td><?php echo $order['validated']; ?></td>
			<td><?php echo $order['project_id']; ?></td>
			<td><?php echo $order['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'orders', 'action' => 'view', $order['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'orders', 'action' => 'edit', $order['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'orders', 'action' => 'delete', $order['id']), null, __('Are you sure you want to delete # %s?', $order['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Tasks'); ?></h3>
	<?php if (!empty($project['Task'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Limit'); ?></th>
		<th><?php echo __('Repeat Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Phase Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Department Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['Task'] as $task): ?>
		<tr>
			<td><?php echo $task['id']; ?></td>
			<td><?php echo $task['name']; ?></td>
			<td><?php echo $task['start']; ?></td>
			<td><?php echo $task['end']; ?></td>
			<td><?php echo $task['limit']; ?></td>
			<td><?php echo $task['repeat_id']; ?></td>
			<td><?php echo $task['created']; ?></td>
			<td><?php echo $task['modified']; ?></td>
			<td><?php echo $task['phase_id']; ?></td>
			<td><?php echo $task['parent_id']; ?></td>
			<td><?php echo $task['project_id']; ?></td>
			<td><?php echo $task['department_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tasks', 'action' => 'view', $task['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tasks', 'action' => 'edit', $task['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tasks', 'action' => 'delete', $task['id']), null, __('Are you sure you want to delete # %s?', $task['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Task'), array('controller' => 'tasks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Taskstypes'); ?></h3>
	<?php if (!empty($project['Taskstype'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Limit'); ?></th>
		<th><?php echo __('Repeat Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Phase Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Department Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($project['Taskstype'] as $taskstype): ?>
		<tr>
			<td><?php echo $taskstype['id']; ?></td>
			<td><?php echo $taskstype['name']; ?></td>
			<td><?php echo $taskstype['start']; ?></td>
			<td><?php echo $taskstype['end']; ?></td>
			<td><?php echo $taskstype['limit']; ?></td>
			<td><?php echo $taskstype['repeat_id']; ?></td>
			<td><?php echo $taskstype['created']; ?></td>
			<td><?php echo $taskstype['modified']; ?></td>
			<td><?php echo $taskstype['phase_id']; ?></td>
			<td><?php echo $taskstype['parent_id']; ?></td>
			<td><?php echo $taskstype['project_id']; ?></td>
			<td><?php echo $taskstype['department_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'taskstypes', 'action' => 'view', $taskstype['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'taskstypes', 'action' => 'edit', $taskstype['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'taskstypes', 'action' => 'delete', $taskstype['id']), null, __('Are you sure you want to delete # %s?', $taskstype['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Taskstype'), array('controller' => 'taskstypes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
