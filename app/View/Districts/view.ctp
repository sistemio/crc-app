<div class="districts view">
	<h2><?php echo __('District'); ?></h2>
	<dl>
	
		<!-- 		
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($district['District']['id']); ?>
					&nbsp;
				</dd>
		 -->		
		 
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($district['District']['name']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div>

<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<h4><?php echo __('Districts'); ?></h4>
	<ul>
		<li><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $district['District']['id'])); ?> </li>
		<li>	
			<?php echo $this->Form->postLink(
                    __('Delete'),
                    array(
                        'action' => 'delete',
                        $district['District']['id']
                        ),
                    null,
                    __(
                        'Are you sure you want to delete # %s?',
                        $district['District']['name']
                    )
                );
			?> 
		</li>
		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('action' => 'add')); ?> </li><br>
	</ul>
		
	<h4><?php echo __('Circuits'); ?></h4>
	<ul>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>

	<div class="related">
			<br>
			<h2><?php echo __('Related circuits'); ?></h2>
			<?php if (!empty($district['Circuit'])): ?>
					<table cellpadding = "0" cellspacing = "0">
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Description'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
						<?php
							$i = 0;
							foreach ($district['Circuit'] as $circuit): ?>
							<tr>
								<td><?php echo $circuit['id']; ?></td>
								<td><?php echo $circuit['name']; ?></td>
								<td class="actions">
									<?php echo $this->Html->link(__('View'), array('controller' => 'circuits', 'action' => 'view', $circuit['id'])); ?>
									<?php echo $this->Html->link(__('Edit'), array('controller' => 'circuits', 'action' => 'edit', $circuit['id'])); ?>
									<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'circuits', 'action' => 'delete', $circuit['id']), null, __('Are you sure you want to delete # %s?', $circuit['name'])); ?>
								</td>
							</tr>
							<?php endforeach; ?>
					</table>
			<?php endif; ?>
	</div>

