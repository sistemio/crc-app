<div class="districts index">
	<h2><?php echo __('Districts'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id', '* Id *'); ?> </th>
			<th><?php echo $this->Paginator->sort('name', '* Description *'); ?></th>
			<th class="actions">
				<?php 
					echo __( 'Actions' );
				?>
			</th>
		</tr>
		
		<?php foreach ($districts as $district): ?>
			<tr>
				<td><?php echo h($district['District']['id']); ?>&nbsp;</td>			
				<td><?php echo h($district['District']['name']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $district['District']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $district['District']['id'])); ?>
					<?php 
						echo $this->Form->postLink(	
                                __('Delete'),
                                array(
                                    'action' => 'delete',
                                    $district['District']['id']
                                ),
								null,
                                __(
                                    'Are you sure you want to delete # %s?',
                                    $district['District']['name']
                                )
                            );
					?> 
					
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	
	<p>
		<?php
			echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
		?>	
	</p>
	
	<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>

<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<ul>
		<h4><?php echo __('Districts'); ?></h4>
		<li><?php echo $this->Html->link(__('Create'), array('action' => 'add')); ?></li><br>
		
		<h4><?php echo __('Circuits'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>
