<div class="districts form">
	<?php echo $this->Form->create('District'); ?>
	<fieldset>
		<legend><?php echo __('Create district'); ?></legend>
		<?php
			echo $this->Form->input('name');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<ul>
		<h4><?php echo __('Districts'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?></li><br>

		<h4><?php echo __('Circuits'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>
