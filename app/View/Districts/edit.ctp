<div class="districts form">
	<?php echo $this->Form->create('District'); ?>
	<fieldset>
		<legend><?php echo __('Edit district'); ?></legend>
		<?php			
			echo $this->Form->input('id');
			echo $this->Form->input('name', array('label' => 'Description'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<ul>
		<h4><?php echo __('Districts'); ?></h4>
		<li>
			<?php 
            echo $this->Form->postLink(
                __('Delete'),
                array(
                    'action' => 'delete', 
                    $this->Form->value('District.id')
                     ), 
                null,
                __(
                    'Are you sure you want to delete # %s?',
                    $this->Form->value('District.name')
                )
            ); 
			?>
		</li>
			
		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?></li><br>

		<h4><?php echo __('Circuits'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
	</ul>
</div>
