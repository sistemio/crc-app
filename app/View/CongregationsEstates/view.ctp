<div class="congregationsEstates view">
<h2><?php echo __('Congregations Estate'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($congregationsEstate['CongregationsEstate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Congregation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($congregationsEstate['Congregation']['name'], array('controller' => 'congregations', 'action' => 'view', $congregationsEstate['Congregation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Estate'); ?></dt>
		<dd>
			<?php echo $this->Html->link($congregationsEstate['Estate']['name'], array('controller' => 'estates', 'action' => 'view', $congregationsEstate['Estate']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Congregations Estate'), array('action' => 'edit', $congregationsEstate['CongregationsEstate']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Congregations Estate'), array('action' => 'delete', $congregationsEstate['CongregationsEstate']['id']), null, __('Are you sure you want to delete # %s?', $congregationsEstate['CongregationsEstate']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Congregations Estates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Congregations Estate'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Congregations'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estates'), array('controller' => 'estates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('controller' => 'estates', 'action' => 'add')); ?> </li>
	</ul>
</div>
