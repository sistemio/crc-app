<div class="circuits form">
	<?php echo $this->Form->create('Circuit'); ?>
	<fieldset>
		<legend><?php echo __('Create circuit'); ?></legend>
		<?php
			echo $this->Form->input('name', array('label' => 'Description'));
			echo $this->Form->input('district_id', array('label' => 'District'));
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>

<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<ul>
		<h4><?php echo __('Circuits'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?></li><br>
		
		<h4><?php echo __('Districts'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>
		
		<h4><?php echo __('Congregations'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>
