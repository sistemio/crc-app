<div class="circuits view">
	<h2><?php echo __('Circuit ... Id('. $circuit['Circuit']['id'].')'); ?></h2>
	
	<dl>
			<!-- 		LAS LINEAS DE A CONTINUACION SE BORRAN
				<dt><?php echo __('Id'); ?></dt> 
				<dd>
					<?php echo h($circuit['Circuit']['id']); ?>
					&nbsp;
				</dd>
			-->
			
			<dt><?php echo __('Desciption'); ?></dt>
			<dd>
				<?php echo h($circuit['Circuit']['name']); ?>
				&nbsp;
			</dd>
			
			<dt><?php echo __('District'); ?></dt>
			<dd>
				<?php echo $this->Html->link(	$circuit['District']['name'], 
																	array('controller' => 'districts', 'action' => 'view', $circuit['District']['id'])	); ?>
				&nbsp;
			</dd>
	</dl>
</div>


<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<ul>
		<h4><?php echo __('Circuits'); ?></h4>
		<li><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $circuit['Circuit']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $circuit['Circuit']['id']), null, __('¿Are you sure you want to delete # %s?', $circuit['Circuit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('action' => 'add')); ?> </li><br>
		
		<h4><?php echo __('Districts'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>
		
		<h4><?php echo __('Congregations'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="related">
	<br><h2><?php echo __('Related congragations'); ?></h2>
	<?php if (!empty($circuit['Congregation'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Circuit Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($circuit['Congregation'] as $congregation): ?>
		<tr>
			<td><?php echo $congregation['id']; ?></td>
			<td><?php echo $congregation['name']; ?></td>
			<td><?php echo $congregation['circuit_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'congregations', 'action' => 'view', $congregation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'congregations', 'action' => 'edit', $congregation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'congregations', 'action' => 'delete', $congregation['id']), null, __('Are you sure you want to delete # %s?', $congregation['name'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<!-- 
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	 -->	
	 
</div>
