<div class="circuits index">
	<h2><?php echo __('Circuits'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id', '* Id *'); ?> </th>
			<th><?php echo $this->Paginator->sort('name', '* Description *'); ?></th>
			<th><?php echo $this->Paginator->sort('district_id', '* District *'); ?></th>
			<th class="actions">
				<?php echo __('Actions'); ?>
			</th>
		</tr>
		
		<?php foreach ($circuits as $circuit): ?>
			<tr>
				<td><?php echo h($circuit['Circuit']['id']); ?>&nbsp;</td>

				<td><?php echo h($circuit['Circuit']['name']); ?>&nbsp;</td> 
				
				<!--  LO QUE HAY DENTRO SE BORRA
						<td>
							<?php echo $this->Html->link(	$circuit['Circuit']['name'],
																				array('controller' => 'circuits', 'action' => 'view', $circuit['Circuit']['id'])	); ?> 
							&nbsp; 
						</td>
				-->

				<td>
					<?php echo $this->Html->link(	'('.
																		$circuit['Circuit']['district_id'].
																		') '.
																		$circuit['District']['name'], 
																		array('controller' => 'districts', 'action' => 'view', $circuit['District']['id'])); ?>
				</td>
				
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $circuit['Circuit']['id'])); ?> 
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $circuit['Circuit']['id'])); ?>
					
					<?php 
                        echo $this->Form->postLink(
                            __('Delete'),
                            array(
                                'action' => 'delete',
                                $circuit['Circuit']['id']
                            ),
                            null,
                            __('Are you sure you want to delete # %s?', $circuit['Circuit']['name'])
                        );
                    ?>
				</td>
			</tr>
		<?php endforeach; ?>
	
	</table>
	
	<p>
		<?php
			echo $this->Paginator->counter(array(
				'format' => __('Pag. {:page} to {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
		?>	
	</p>
	
	<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>

<div class="actions">
	<h3><?php echo __('Options'); ?></h3>
	<ul>
		<h4><?php echo __('Circuits'); ?></h4>
		<li><?php echo $this->Html->link(__('Create'), array('action' => 'add')); ?></li><br>

		<h4><?php echo __('Districts'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>

		<h4><?php echo __('Congregations'); ?></h4>
		<li><?php echo $this->Html->link(__('List'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Create'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>
