<div class="circuits form">
    <?php echo $this->Form->create('Circuit'); ?>
    <fieldset>
        <legend><?php echo __('Edit circuit'); ?></legend>
        <?php
            echo $this->Form->input('id');
            echo $this->Form->input('name');
            echo $this->Form->input('district_id');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Save')); ?>
</div>

<div class="actions">
    <h3><?php echo __('Options'); ?></h3>
    <ul>

        <h4><?php echo __('Circuits'); ?></h4>
        <li>
            <?php 
                echo $this->Form->postLink(
                    __('Delete'), 
                    array(
                        'action' => 'delete', 
                        $this->Form->value('Circuit.id')
                    ),
                    null,
                    __(
                        'Are you sure you want to delete # %s?', 
                        $this->Form->value('Circuit.id')
                    )
                ); 
            ?>
        </li>
        <li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?></li><br>
        <h4><?php echo __('Districts'); ?></h4>
        <li><?php echo $this->Html->link(__('List'), array('controller' => 'districts', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('Create'), array('controller' => 'districts', 'action' => 'add')); ?> </li><br>
        <h4><?php echo __('Congregations'); ?></h4>
        <li><?php echo $this->Html->link(__('List'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('Create'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
    </ul>
</div>
