<div class="congregations form">
<?php echo $this->Form->create('Congregation'); ?>
	<fieldset>
		<legend><?php echo __('Edit Congregation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('circuit_id');
		echo $this->Form->input('Estate');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Congregation.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Congregation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Congregations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Circuits'), array('controller' => 'circuits', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Circuit'), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estates'), array('controller' => 'estates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate'), array('controller' => 'estates', 'action' => 'add')); ?> </li>
	</ul>
</div>
